This is a basic auto-install script which can be used to install **Sympl** with minimal intervention, and point the user to important resources.


## Install Stable/Production Version

```bash
wget https://gitlab.com/sympl.io/install/raw/master/install.sh
```
```bash
bash install.sh
```

## Install Beta/Testing Version

```bash
wget https://gitlab.com/sympl.io/install/raw/master/install.sh
```
```bash
bash install.sh --testing
```

## Automated Installs

For automated and scripted installs, the `--noninteractive` flag can be added.
Note that this disables the welcome message and password changes, however it is still strongly recommended to set a unique strong password for the `sympl` user, and we ask that new users are pointed to http://wiki.sympl.io/Get_Started at the very least.