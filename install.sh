#!/bin/bash

set -e

if [ $( id -u $(whoami) ) -ne 0 ]; then
  echo "Sorry, this must be run as root."
  exit 1
fi

noninteractive=0
codename=$( if ! which lsb_release > /dev/null ; then grep ^VERSION= /etc/os-release | cut -d \( -f 2 | cut -d \) -f 1 ; else lsb_release -c | cut -f 2 ; fi )
version=$codename
UNHANDLED=()

if id -u sympl > /dev/null 2>&1 ; then noninteractive=1; fi

while [ $# -gt 0 ]; do
  case $1 in
    --unstable)
      version="$codename-unstable"
      shift
    ;;
    --testing)
      version="$codename-testing"
      shift
    ;;
    --noninteractive)
      noninteractive=1
      shift
    ;;
    *)
      UNHANDLED+=("$1")
      shift
    ;;
  esac
done
if [ "x$UNHANDLED" != "x" ]; then
  echo "Error: Unknown parameter '$UNHANDLED'"
  exit 1
fi

clear
echo
echo "-----------------------------------------------------------------------"
echo "                     Sympl Installer    v20240223                      "
echo "-----------------------------------------------------------------------"
echo
echo "This script will help you install Sympl on a Debian Linux or Raspberry"
echo "  Pi OS server with minimal hassle, and give you some intial pointers."
echo

if [ $( grep -c '^buster$\|^bullseye$\|^bookworm$' <<< $codename ) == 0 ]; then
  echo "ERROR: Sorry, this script doesn't recognise your Debian version."
  echo "  You may need to install lsb_release, or check your version."
  echo "  Expected 'buster', 'bullseye' or 'bookworm' but got '$codename'."
  exit 1
fi

if ! hostname --fqdn > /dev/null 2>&1 ; then
  echo "ERROR: Something doesn't seem right with your server hostname."
  echo "  You should check it has a matching enty in /etc/hosts and is"
  echo "  set properly before continuing, as Sympl uses the hostname."
  echo "  Check it's current setting with 'hostname -f', and if using a"
  echo "  public domain, ensure that is valid and pointing to the server."
  exit 1
fi

export DEBIAN_FRONTEND=noninteractive
export APT_LISTCHANGES_FRONTEND=none

echo -n "Installing initial dependencies..."
apt -qqq -y update

if [ $( dpkg -l | grep -c '^ii *gnupg[ 12]' ) -eq 0 ]; then
  apt -qqq -y install gnupg
  if [ $( dpkg -l | grep -c '^ii *gnupg[ 12]' ) -eq 0 ]; then
    echo "ERROR: Unable to install GnuPG, you will need to install manually, sorry."
    exit 1
  fi
fi

if [ "x$(which debconf-set-selections)" = "x" ] ; then
  apt -qqq -y install debconf
  if [ "x$(which debconf-set-selections)" = "x" ]; then
    echo "ERROR: Unable set defaults, you will need to install manually, sorry."
    exit 1
  fi
fi

# If we're running Debian Buster, then add the backports repo so we can get to phpMyAdmin there
# We don't need to do this with Raspbian/Raspberry Pi OS as it's in the normal repo there
if [ "$(lsb_release -i -s)" == "Debian" ] && [ "$codename" == "buster" ]; then
  if [ $( cat /etc/apt/sources.list /etc/apt/sources.list.d/*.list | sed 's|#.*||' | grep -c '^deb .* buster-backports' ) -lt 1 ]; then
    echo -n 'Enabling Debian buster-backports repo for phpmyadmin...'
    echo "deb http://deb.debian.org/debian buster-backports main" > /etc/apt/sources.list.d/enable_backports.list
    echo ' ok'
  fi
  if [ ! -f /etc/apt/preferences.d/sympl-phpmyadmin ]; then
    echo -n 'Enabling install of phpmyadmin from backports...'
    echo -e "Package: phpmyadmin\nPin: release a=buster-backports\nPin-Priority: 900" > /etc/apt/preferences.d/sympl-phpmyadmin
    echo -e "Package: php-twig\nPin: release a=buster-backports\nPin-Priority: 900" >> /etc/apt/preferences.d/sympl-phpmyadmin
	echo ' ok'
  fi
fi

echo ' OK'

echo -n "Setting defaults..."
echo "phpmyadmin phpmyadmin/reconfigure-webserver select apache2" | debconf-set-selections
echo "roundcube-core roundcube/dbconfig-install boolean true" | debconf-set-selections
echo "roundcube-core roundcube/database-type select mysql" | debconf-set-selections
echo "roundcube-core roundcube/mysql/app-pass password" | debconf-set-selections
echo "roundcube-core roundcube/reconfigure-webserver select apache2" | debconf-set-selections
echo "iptables-persistent	iptables-persistent/autosave_v4	boolean	true" | debconf-set-selections
echo "iptables-persistent	iptables-persistent/autosave_v6	boolean	true" | debconf-set-selections
echo ' OK'

echo -n "Adding repository key..."
if [ $( grep -c '^buster$\|^bullseye$' <<< $codename ) -ne 0 ]; then
  # use apt-key add
  wget -qO- https://mirror.mythic-beasts.com/mythic/support@mythic-beasts.com.gpg.key | apt-key add - 2>/dev/null
else
  # use newer /etc/apt/trusted.gpg.d
  wget -q https://mirror.mythic-beasts.com/mythic/support@mythic-beasts.com.gpg.key -O /etc/apt/trusted.gpg.d/support@mythic-beasts.com.asc
fi
echo ' OK'

if [ ! -f /var/swap ] && [ $( grep -c -i 'swap' /etc/fstab ) == 0 ] && [ $( swapon -s | wc -l ) = 0 ] ; then
    echo -n "No swap space allocated, allocating 1GB swap at /var/swap..."
    if ! fallocate -l1G /var/swap 2> /dev/null ; then
        dd if=/dev/zero of=/var/swap bs=1024 count=1048576 status=none
    fi
    chmod 600 /var/swap
    chown root:root /var/swap
    mkswap /var/swap > /dev/null
    echo "/var/swap swap swap defaults 0 0" >> /etc/fstab
    echo "1" > /proc/sys/vm/swappiness
    echo "vm.swappiness = 1" > /etc/sysctl.d/sympl-swappiness
    swapon -a
    echo ' OK'
fi

# create apt/sources-list.d if needed.
mkdir -p /etc/apt/sources.list.d/

if [ "$( find /etc/apt/sources.list.d/ -mindepth 1 -maxdepth 1 -type f -name 'sympl_*.list' | wc -l )" != "0" ]; then
   if [ "$( grep -c '^deb http://packages.mythic-beasts.com/mythic/' /etc/apt/sources.list.d/* )" != "0" ]; then
     echo -n "Removing previous Sympl repo..."
     sed -i 's|^deb http://packages.mythic-beasts.com/mythic/.*|#&|' /etc/apt/sources.list.d/*
     find /etc/apt/sources.list.d/ -mindepth 1 -maxdepth 1 -type f -name 'sympl_*.list' -delete
     echo " OK"
   fi
fi

echo -n "Adding repository to apt..."
echo "deb http://packages.mythic-beasts.com/mythic/ $version main" > "/etc/apt/sources.list.d/sympl_$codename.list"
apt -qqq -y update
echo " OK"

echo "Installing Sympl from '$version' repository..."
if [ $noninteractive == 0 ]; then
  echo "Starting in 5 seconds - Ctrl-C to cancel!"
  sleep 5
fi

apt -y install --install-recommends sympl-core

if [ $? != 0 ]; then
  echo "Something went wrong. Exiting."
  exit 1
fi

apt -qqq -y upgrade

clear
echo -n "Thanks for installing..."
/etc/update-motd.d/00-sympl-banner

SYMPL_PASSWORD="$(openssl rand -base64 32 | tr -dc A-Za-z0-9 | cut -c 1-16)"
echo
echo -n "Setting password for user 'sympl' to '$SYMPL_PASSWORD'..."
echo "sympl:$SYMPL_PASSWORD" | /usr/sbin/chpasswd
echo ' OK'

/etc/update-motd.d/00-sympl-banner | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" > /home/sympl/README_SYMPL.txt
chown sympl:sympl /home/sympl/README_SYMPL.txt
chmod 600 /home/sympl/README_SYMPL.txt

echo "
                         Important Information

 1. This system now has a system user named 'sympl', which has inherited the
      ssh keys from the root user if set.
    A randomly generated password for the 'sympl' user has been set as:
      $SYMPL_PASSWORD
    You should change this password as soon as possible with the command
      'passwd sympl'.
    Ensure the password is complex as it has access to the whole system.
    Consider using SSH Keys where possible.

 2. If you need to access MySQL/MariaDB, there a 'sympl' user hase been
      created for that purpose.
    You can use the 'mysql' command without specifying a username/password.
    If you need it, the username is 'sympl', and password is in
      /home/sympl/mysql_password, but you should not use this anywhere else,
      and instead create individual users as needed.

 3. Documentation is at https://wiki.sympl.io.
    Help can be found at https://forum.sympl.io.
    If you encounter a bug, you can log it at https://bugs.sympl.io.

 4. To get started, check http://wiki.sympl.io/Get_Started

(Press 'q' to quit less.)" >> /home/sympl/README_SYMPL.txt

if [ ! -f /root/README_SYMPL.txt ] && [ ! -L /root/README_SYMPL.txt ] ; then ln -s /home/sympl/README_SYMPL.txt /root/README_SYMPL.txt ; fi

echo "Important new-user information is available at any time by running:
    less ~/README_SYMPL.txt
    You should read this as soon as possible.

Check these URLs for other information:
    http://wiki.sympl.io for documentation and guides.
    http://forum.sympl.io for support and assistance.
    "
